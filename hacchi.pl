#!/usr/bin/env perl
use strict;
use warnings;

use POE qw(Component::IRC);
use Time::Duration;
use LWP::UserAgent;

my $nickname = 'hacchi';
my $ircpassword = '';
my $ircname  = 'Patch-tracking bot for Exherbo';
my $server   = 'irc.freenode.net';

my @channels = ( '#exherbo', '#exherbo-dev' );
my @slackerchannels = ( '#exherbo' );

my @patches;

my $irc = POE::Component::IRC->spawn(
    nick    => $nickname,
    ircname => $ircname,
    server  => $server
) or die "Oh noooo! $!";

POE::Session->create(
    package_states =>
     [ main => [qw(_default _start irc_001 irc_public irc_msg slackeralert)], ],
    heap => { irc => $irc },
);

$poe_kernel->run();

sub _start {
    my $heap = $_[HEAP];

    my $irc = $heap->{irc};

    $irc->yield( register => 'all' );
    $irc->yield( connect  => {} );
    return;
}

sub irc_001 {
    my $sender = $_[SENDER];

    my $irc = $sender->get_heap();

    print "Connected to ", $irc->server_name(), "\n";
    $irc->yield( privmsg => 'nickserv' => "identify $ircpassword" );

    $irc->delay( [ join => $_ ], 5 ) for @channels;
    $_[KERNEL]->delay( 'slackeralert', 3600 );
    return;
}

sub irc_public {
    my ( $sender, $who, $where, $what ) = @_[ SENDER, ARG0 .. ARG2 ];
    my $nick = ( split /!/, $who )[0];
    my $channel = $where->[0];
    my $saneurl = "";

    $what =~ s/\s+$//;
    if ( my ($url) = $what =~ /^!(?:patchqueue|pq)\s+(.+)/ ) {
        my $summary = "";

        if (my ( $id ) = ($url =~ qr|^http://dpaste.com/(\d+)(?:/(?:plain(?:/)?)?)?(\s+.*)?$|)) {
            $saneurl = "http://dpaste.com/$id/plain";
        }
        elsif (( $id ) = ($url =~ qr|^http://ix.io/(\w+)|)) {
            $saneurl = "http://ix.io/$id";
        }
        elsif (( $id ) = ($url =~ qr|^http://paste.pocoo.org/(?:show\|raw)/(\d+)|)) {
            $saneurl = "http://paste.pocoo.org/raw/$id";
        }

        if ($saneurl) {
            my $ua = LWP::UserAgent->new;
            my $response = $ua->get($saneurl);
            if ($response->is_success) {
                my $subject = "";
                my @lines = split(/\n/, $response->decoded_content);
                foreach my $line (@lines[0..9]) {
                    last unless $line;
                    last if ( $subject ) = ($line =~ /^Subject: (.+)$/);
                }
                $summary = $subject if $subject;
            }
            else {
                $irc->yield( privmsg => $channel => ("Got HTTP " . $response->code() . " for $saneurl") );
            }
        }

        push @patches, { nick => $nick, time => time, url => $url, summary => $summary };
        $summary = "Queued '" . $summary . "'. " if $summary;
        $irc->yield( privmsg => $channel => $summary . "Now " . @patches . " "
           . pluralise_patch(scalar @patches)
           . " in queue" );
    }

    if ( my ($match) = $what =~ /^!(?:patchlist|pl)(?:\s+(.+))?$/ ) {
        my @p = @patches;
        if ($match) {
            @p = grep { ($_->{nick} =~ /\Q$match\E/ or $_->{url} =~ /\Q$match\E/ or $_->{summary} =~ /\Q$match\E/ ) } @p;
        }
        my $msg = @p . " ";
        $msg .= "matching " if ($match);
        $msg .= pluralise_patch(scalar @p) . " in queue";
        $irc->yield( privmsg => $channel => $msg );

        if ( @p <= 3 ) {
            # Send them to the public channel
            foreach my $patch (@p) {
                send_patch( $patch, $channel );
            }
        } else {
            # Send the three first patches to the channel and all to the user
            foreach my $patch ( @p[ 0 .. 2 ] ) {
                send_patch( $patch, $channel );
            }

            $irc->yield( privmsg => $nick => "Patch list:" ) if (@p);
            foreach my $patch (@p) {
                send_patch( $patch, $nick );
            }
        }
    }

    if ( my ($match) = $what =~ /^!(?:patchdone|pd)\s+(.+)/ ) {
        my @tmp;
        foreach my $patch (@patches) {
            unless ( $patch->{nick}    =~ /\Q$match\E/
                  or $patch->{url}     =~ /\Q$match\E/
                  or $patch->{summary} =~ /\Q$match\E/ )
            {
                push @tmp, $patch;
            }
        }
        if ( @patches == @tmp ) {
            $irc->yield( privmsg => $channel => "No patches found matching '"
              . $match
              . "' :(" );
        } else {
            my $msg = "Marked " . scalar(@patches - @tmp) . " "
                        . pluralise_patch(@patches - @tmp) . " done. "
                        . @tmp . " " . pluralise_patch(scalar @tmp)
                        . " left in queue.";
            $irc->yield( privmsg => $channel => $msg );
        }
        @patches = @tmp;
    }

    if ( $what =~ /^!(patchalldone|pad)$/ ) {
        @patches = qw();
        $irc->yield( privmsg => $channel => "All patches marked done :)" );
    }

    if ( $what =~ /^!h(?:e(?:lp)?)?$/ ) {
        $irc->yield( privmsg => $channel => "Read: http://ossowicki.com/?p=213");
    }

    return;
}

sub irc_msg {
    my ( $sender, $who, $where, $what ) = @_[ SENDER, ARG0 .. ARG2 ];
    my $nick = ( split /!/, $who )[0];

    if ( my ($match) = $what =~ /^!(?:patchlist|pl)(?:\s+(.+))?$/ ) {
        my @p = @patches;
        if ($match) {
            @p = grep { ($_->{nick} =~ /\Q$match\E/ or $_->{url} =~ /\Q$match\E/ or $_->{summary} =~ /\Q$match\E/ ) } @p;
        }
        my $msg = @p . " ";
        $msg .= "matching " if ($match);
        $msg .= pluralise_patch(scalar @p) . " in queue";
        $irc->yield( privmsg => $nick => $msg );

        foreach my $patch (@p) {
            send_patch( $patch, $nick );
        }
    }

    if ( $what =~ /^!?(?:patchqueue|pq) (.+)/ ) {
        $irc->yield(
            privmsg => $nick => "Please submit patches in a public channel" );
    }

    if ( $what =~ /^!?(?:patch(?:all|)done|pd|pad) (.+)/ ) {
        $irc->yield(
            privmsg => $nick => "Please mark patches done in a public channel" );
    }

    if ( $what =~ /^!?h(?:e(?:lp)?)?$/ ) {
        $irc->yield( privmsg => $nick => "Read: http://ossowicki.com/?p=213");
    }

}

sub slackeralert {
    if (@patches) {
        my %repo;
        foreach my $p (@patches) {
            if ( $p->{url} =~ /^(?:.*\s+|)(::\S+)/ ) {
                ++$repo{$1};
            }
        }
        my $msg = @patches . " " . pluralise_patch(scalar @patches) . " in queue... slackers!";

        $msg .= " (" if (scalar keys %repo);
        foreach my $k (keys %repo) {
            $msg .= $repo{$k} . " in " . $k . ". ";
        }
        $msg .= ")" if (scalar keys %repo);
        $msg =~ s/\. \)$/\)/; # eww


        foreach my $channel (@slackerchannels) {
            $_[HEAP]->{irc}->yield( privmsg => $channel => $msg );
        }
    }
    $_[KERNEL]->delay( 'slackeralert', 3600 );
}

sub _default {
    my ( $event, $args ) = @_[ ARG0 .. $#_ ];

    # Debug info
    my @output = ("$event: ");
    for my $arg (@$args) {
        if ( ref $arg eq 'ARRAY' ) {
            push( @output, '[' . join( ', ', @$arg ) . ']' );
        }
        else {
            push( @output, "'$arg'" );
        }
    }
    print join ' ', @output, "\n";
    return 0;
}

sub send_patch {
    my ( $patch, $target ) = @_;
    print "TARGET: $target\n";
    my $age = time - $patch->{time};
    $irc->yield( privmsg => $target => " "
        . $patch->{url}
        . " (submitted by "
        . $patch->{nick} . " "
        . ago($age)
        . ") "
        . $patch->{summary});
}

sub pluralise_patch {
    if ( shift == 1 ) {
        return 'patch';
    }
    return 'patches';
}

# vim: set tw=120 sw=4 sts=4 et :
